"""
debug_msg : prints the message (msg) if background_message is set to True, otherwise, the message is ignored.
"""

from config import background_messages


def debug_msg(msg):
    if background_messages:
        print(msg)
