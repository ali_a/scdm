#!/usr/bin/env python3

"""

"""

from PyQt5.QtWidgets import QWidget, QApplication, QHBoxLayout, QVBoxLayout, QPushButton, QLabel, QScrollArea
from PyQt5.QtCore import QRect


class ServicePage(QWidget):
    def __init__(self):
        super().__init__()

        # buttons container
        self.exit_button = QPushButton('&Exit')
        self.start_button = QPushButton('&Start')
        self.stop_button = QPushButton('&Stop')
        self.buttons_HBox = QHBoxLayout()
        self.buttons_HBox.addWidget(self.start_button)
        self.buttons_HBox.addWidget(self.stop_button)
        self.buttons_HBox.addWidget(self.exit_button)
        self.start_button.setEnabled(False)
        self.stop_button.setEnabled(False)

        # aria2 status
        self.status_prefix = 'Aria2 RPC Status : '
        self.status_label = QLabel(self.status_prefix + 'Unknown')
        self.status_HBox = QHBoxLayout()
        self.status_HBox.addWidget(self.status_label)

        # users status
        self.scrollArea = QScrollArea(self)
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget(self.scrollArea)
        self.scrollAreaWidgetContents.setGeometry(QRect(0, 0, 380, 247))
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.verticalLayoutScroll = QVBoxLayout(self.scrollAreaWidgetContents)

        # vertical box (page container)
        self.page_container = QVBoxLayout()
        self.page_container.addLayout(self.buttons_HBox)
        self.page_container.addLayout(self.status_HBox)
        self.page_container.addWidget(self.scrollArea)

        # init page with created contents
        self.setLayout(self.page_container)
        self.setWindowTitle('Service Page')

        # TODO: move self.show() out of __init__.
        self.show()

    def aria2_status_show_online(self):
        self.status_label.setText(self.status_prefix + 'Online')

    def aria2_status_show_offline(self):
        self.status_label.setText(self.status_prefix + 'Offline')

    def update_users_status(self, users_status):
        # clean current content
        for user in range(self.verticalLayoutScroll.count()):
            self.verticalLayoutScroll.itemAt(user).widget().deleteLater()
        # add new statuses
        for user_status in users_status:
            self.verticalLayoutScroll.addWidget(QLabel(user_status))


if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    Service = ServicePage()
    status = ['user1 : Halted', 'user2 : Downloading', 'admin : Disabled']
    Service.update_users_status(status)
    app.exec_()

