#!/usr/bin/env python3

"""

"""

import sys
from Interface_main import MainPage
from PyQt5.QtWidgets import QApplication


class Main:
    def __init__(self, user_id):
        self.user_id = user_id
        self.main_page = MainPage()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    Main = Main(1)
    app.exec_()
