#!/usr/bin/env python3

"""
Authenticate the user's info from DB.

Authenticate class's method(s):

    1. user: get users info ['username', 'password'] and returns:
         0      : if username doesn't exists in DB. password is ignored.
        -1      : if the password doesnt match the user's password inside DB.
        user_id : if username exists in DB and the password is correct.
"""

from Database_get_user_info import GetUserFromDatabase
from Business_debugger import debug_msg


class Authenticate:
    @staticmethod
    def user(username, password):
        user_info_from_database = GetUserFromDatabase.get_user_info(username)

        if user_info_from_database is None:
            debug_msg('Auth: user "{username}" not found in DB.'.format(username=username))
            return 0

        if user_info_from_database[1] == username and user_info_from_database[2] == password:
            debug_msg('Auth: user "{username}" identified.'.format(username=username))
            return user_info_from_database[0]

        else:
            debug_msg('Auth: wrong password for "{username}"'.format(username=username))
            return-1

    @staticmethod
    def is_user_in_db(username):
        user_info_from_database = GetUserFromDatabase.get_user_info(username)

        if user_info_from_database is None:
            return False
        else:
            return True


if __name__ == '__main__':
    print(Authenticate.user('ali', '1234'))
