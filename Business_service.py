#!/usr/bin/env python3

"""

"""

from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QTimer
from Business_debugger import debug_msg
from Interface_service import ServicePage
from Business_downloader import aria2_start, aria2_kill, is_aria2rpc_running
from Database_get_user_info import get_all_users_info
from Business_user import User


class Service:
    def __init__(self, time_interval=1000):
        self.service_page = ServicePage()

        self.service_page.exit_button.pressed.connect(self.exit_action)
        self.service_page.start_button.pressed.connect(self.start_action)
        self.service_page.stop_button.pressed.connect(self.stop_action)

        if is_aria2rpc_running():
            self.start_action()

        self.all_users = self.create_users(get_all_users_info())
        self.all_users_status = None
        self.update_users_status()

        self.timer = QTimer()
        self.timer.setInterval(time_interval)
        self.timer.timeout.connect(self.tick)
        self.timer.start()

    def update_users_status(self):
        status = []
        for user in self.all_users:
            status.append(user.get_status())
        self.all_users_status = status

    @staticmethod
    def create_users(users_info_list):
        users_list = []
        for user in users_info_list:
            users_list.append(User(user))
        return users_list

    def tick(self):
        self.update_aria2_status()
        for user in self.all_users:
            user.tick()
        self.update_users_status()
        self.service_page.update_users_status(self.all_users_status)

    def exit_action(self):
        self.stop_action()
        debug_msg('exited from service page.')
        sys.exit()

    def start_action(self):
        debug_msg('calling start from service')
        aria2_start()
        self.update_aria2_status()

    def stop_action(self):
        debug_msg('calling stop from service')
        aria2_kill()
        self.update_aria2_status()

    def update_aria2_status(self):
        if is_aria2rpc_running():
            self.service_page.aria2_status_show_online()
            self.service_page.start_button.setEnabled(False)
            self.service_page.stop_button.setEnabled(True)
        else:
            self.service_page.aria2_status_show_offline()
            self.service_page.start_button.setEnabled(True)
            self.service_page.stop_button.setEnabled(False)


if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    Service = Service()
    app.exec_()
