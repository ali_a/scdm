#!/usr/bin/env python3

"""
LoginPage class is a GUI for login page.
LoginPage's methods:
    0. __init__ : there is a lot to explain, hmm.
    1. get_username : returns the content of username_line_edit
    2. get_password :returns the content of password_line_edit
    3. prompt_user_does_not_exists : prompt the user that 'user does not exists'
    4. prompt_wrong_password : prompt the user that 'wrong password'
"""

from PyQt5.QtWidgets import QWidget, QLineEdit, QLabel, QPushButton, QVBoxLayout, QHBoxLayout, QApplication


class LoginPage(QWidget):
    def __init__(self):
        super().__init__()

        # username container
        self.username_label = QLabel('Username :')
        self.username_line_edit = QLineEdit()
        self.username_HBox = QHBoxLayout()
        self.username_HBox.addWidget(self.username_label)
        self.username_HBox.addWidget(self.username_line_edit)

        # password container
        self.password_label = QLabel('Password :')
        self.password_line_edit = QLineEdit()
        self.password_line_edit.setEchoMode(QLineEdit.Password)
        self.password_HBox = QHBoxLayout()
        self.password_HBox.addWidget(self.password_label)
        self.password_HBox.addWidget(self.password_line_edit)

        # buttons container
        self.exit_button = QPushButton('&Exit')
        self.register_button = QPushButton('&Register')
        self.login_button = QPushButton('&Login')
        self.buttons_HBox = QHBoxLayout()
        self.buttons_HBox.addWidget(self.exit_button)
        self.buttons_HBox.addWidget(self.register_button)
        self.buttons_HBox.addWidget(self.login_button)

        # message box container
        self.message_box_label = QLabel('Welcome to SCDM!')
        self.message_box_HBox = QHBoxLayout()
        self.message_box_HBox.addWidget(self.message_box_label)

        # vertical box (page container)
        self.page_container = QVBoxLayout()
        self.page_container.addLayout(self.username_HBox)
        self.page_container.addLayout(self.password_HBox)
        self.page_container.addLayout(self.buttons_HBox)
        self.page_container.addLayout(self.message_box_HBox)

        # init page with created contents
        self.setLayout(self.page_container)
        self.setWindowTitle('Login Page')

        self.username_line_edit.returnPressed.connect(self.password_line_edit.setFocus)
        self.password_line_edit.returnPressed.connect(self.login_button.setFocus)

        # TODO: move self.show() out of __init__. somewhere like Business_Hub.py
        self.show()

    def get_username(self):
        return self.username_line_edit.text()

    def get_password(self):
        return self.password_line_edit.text()

    def prompt_user_does_not_exists(self):
        self.message_box_label.setText('user does not exists')

    def prompt_wrong_password(self):
        self.message_box_label.setText('wrong password')

    def prompt_user_registered(self):
        self.message_box_label.setText('user registered. you may login now.')

    def prompt_user_exists_and_register_rejected(self):
        self.message_box_label.setText('user already exists!')


if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    LoginPage = LoginPage()
    app.exec_()
