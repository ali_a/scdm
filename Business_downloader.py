#!/usr/bin/env python3

"""

"""

import xmlrpc.client
from config import PORT, XML_RPC_ADDRESS, TEST_DOWNLOAD_URL
from os import system
from time import sleep, strptime, strftime, time
import subprocess

server = xmlrpc.client.ServerProxy(XML_RPC_ADDRESS, allow_none=True).aria2


def aria2_start():
    system('aria2c --no-conf  --enable-rpc --rpc-listen-port ' + str(PORT) +
           ' --rpc-max-request-size=2M --rpc-listen-all --quiet=true &')
    sleep(0.1)


def aria2_kill():
    system('pkill aria2')
    sleep(0.1)


def aria2_version():
    sleep(0.1)
    return server.getVersion()


def time_activation(start_time, stop_time):
    """
    Given the start time and stop time, this function will say if current time is between start and stop time. The
    answer is returned as True or False.
    """
    current_time = strptime(strftime('%H:%M:%S'), '%H:%M:%S')
    if start_time <= current_time <= stop_time:
        return True
    else:
        return False


def is_aria2rpc_running():
    pgrep_process = subprocess.Popen('pgrep -l aria2', shell=True, stdout=subprocess.PIPE)
    if pgrep_process.stdout.readline() == b'':
        return False
    else:
        return True


def add_uri(url):
    answer = server.addUri([url])
    return answer


def tell_status(gid):
    answer = server.tellStatus(gid)
    return answer


if __name__ == '__main__':
    print('starting aria2')
    aria2_start()
    print('version : ' + str(aria2_version()))
    ans = add_uri(TEST_DOWNLOAD_URL)
    print('queued : ' + ans)
    status = tell_status(ans)
    print(status)

