#!/usr/bin/env python3

"""

"""

import Database_do_select_query


def get_ready_list(user_ID):
    query = 'SELECT * FROM links WHERE owner = {user_ID} and status = "READY"'.format(user_ID=user_ID)
    return [list(i) for i in Database_do_select_query.DoSelectQuery(query).fetch_all()]


if __name__ == '__main__':
    print(get_ready_list(4))
