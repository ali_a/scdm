#!/usr/bin/env python3

"""
take a username and a password as input and insert it to database
"""

from Database_do_insert_query import DoInsertQuery
from Business_debugger import debug_msg


class InsertUserToDatabase:

    @staticmethod
    def insert_user(username, password):
        query = 'INSERT INTO users VALUES(NULL, "{username}", "{password}")'\
                .format(username=username, password=password)
        try:
            DoInsertQuery(query)
        except ValueError as error:
            debug_msg(error)
            return -1
        return 0


if __name__ == '__main__':
    print(InsertUserToDatabase().insert_user('ali', '1234'))
