#!/usr/bin/env python3

"""

"""

import Database_get_link_list
import Business_downloader as DownloadManager
from time import strptime, strftime
from Business_debugger import debug_msg
from Database_update_link_status import set_status_to_done


class User:
    def __init__(self, user_info):
        self.ID = user_info[0]
        self.username = user_info[1]
        self.telegram_ID = user_info[3]
        self.schedule_start_time = strptime(user_info[4], '%H:%M:%S')
        self.schedule_stop_time = strptime(user_info[5], '%H:%M:%S')
        self.queue_activation = user_info[6]

        self.current_download = None
        self.queue_list = self.return_links_from_database()
        # status: 'Downloading', 'Stopped', 'Waiting', 'Offline'
        self.status = 'Stopped'

    def return_links_from_database(self):
        return Database_get_link_list.get_ready_list(self.ID)

    def get_status(self):
        return self.username + ' : ' + self.status

    def show_user_info(self):
        print('ID : ' + str(self.ID))
        print('username : ' + self.username)
        print('telegram ID : ' + self.telegram_ID)
        print('start time : ' + strftime('%H:%M:%S', self.schedule_start_time))
        print('stop time : ' + strftime('%H:%M:%S', self.schedule_stop_time))
        print('queue activation : ' + str(self.queue_activation))
        print('current download : ' + str(self.current_download))
        print('status : ' + str(self.status))
        print('Queue :')
        for link in self.queue_list:
            print(str(link))
        print('')

    def start_downloading(self, gid):
        for link in self.queue_list:
            if link[0] == gid:
                DownloadManager.add_uri(link[2])
                self.current_download = gid
                return

    def tick(self):
        previous_status = self.status
        time_activation = DownloadManager.time_activation(self.schedule_start_time, self.schedule_stop_time)

        # update status
        if DownloadManager.is_aria2rpc_running() is False:
            self.status = 'Offline'
        elif self.queue_activation == 0:
            self.status = 'Stopped'
        elif self.current_download is None and self.queue_list.__len__() == 0:
            self.status = 'Done.'
        elif self.queue_activation == 1 and time_activation is True:
            self.status = 'Downloading'
        elif self.queue_activation == 1 and time_activation is False:
            self.status = 'Waiting'
        else:
            self.status = 'Unknown'

        # actions needed to be taken when status has changed
        # TODO: handle Unknown status here

        # when downloading, if current download is empty, it will assign a link to it and start downloading
        if self.status == 'Downloading' and self.current_download is None and self.queue_list.__len__() != 0:
            self.current_download = DownloadManager.add_uri(self.queue_list[0][1])
            debug_msg('user ' + self.username + ' started ' + str(self.current_download) +
                      ' pointing to ' + self.queue_list[0][1])
        if self.status == 'Downloading' and self.current_download is not None:
            full_status = DownloadManager.tell_status(self.current_download)
            status = full_status['status']
            if status == 'complete':
                # TODO: check current download as Done
                debug_msg('user ' + self.username + ' downloaded ' + str(self.current_download) +
                          ' pointing to ' + self.queue_list[0][1])
                set_status_to_done(self.queue_list[0][0])
                del self.queue_list[0]
                self.current_download = None


if __name__ == '__main__':
    from Database_get_user_info import get_user_info
    user = User(get_user_info(1))
    user.show_user_info()
    DownloadManager.aria2_start()

'''
    for tick in range(0, 10):
        print('tick : ' + str(tick))
        print(user.current_download)
        user.tick()
        sleep(1)
'''