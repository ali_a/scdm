#!/usr/bin/env python3

from PyQt5.QtWidgets import QTableWidget, QTableWidgetItem, QApplication, QWidget, QHBoxLayout, QVBoxLayout


class MainPage(QWidget):
    def __init__(self):
        super().__init__()

        self.table = QTableWidget()
        self.table.setRowCount(5)
        self.table.setColumnCount(2)
        self.table_container = QHBoxLayout()
        self.table_container.addWidget(self.table)

        self.page_container = QVBoxLayout()
        self.page_container.addLayout(self.table_container)

        self.setWindowTitle('welcome')
        self.setLayout(self.page_container)

        self.show()


if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    Main = MainPage()
    app.exec_()
