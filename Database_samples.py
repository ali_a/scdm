#!/usr/bin/env python3

"""
inserts some samples to database. for now, only users samples are available.
TODO : use DatabaseDoInsertQuery when its available
TODO : insert some samples to query table when its possible
"""

import time
import sqlite3
from config import DATABASE_FULL_ADDRESS
from Business_debugger import debug_msg


def main():
    user_1 = ('NULL', 'admin', 'admin', 'tel1', time.strftime("%H:%M:%S"), '16:42:08', 0)
    user_2 = ('NULL', 'ali', '1234', 'telID2', '21:22:23', '00:00:01', 1)
    user_3 = ('NULL', 'alice', '4321AA', '981255', '00:00:00', '00:00:00', 0)
    user_4 = ('NULL', 'greedy_man', 'evil', '', '00:00:00', '23:59:59', 1)
    users = [user_1, user_2, user_3, user_4]

    url_1 = 'https://forum.ubuntu.ir/Themes/ubuntu-ir-theme/images/footer_logo_ir.png'
    url_2 = 'https://forum.ubuntu.ir/index.php?action=dlattach;attach=31477;type=avatar'
    url_3 = 'https://forum.ubuntu.ir/Themes/ubuntu-ir-theme/images/logo_ir.png'
    url_4 = 'https://www.tutorialspoint.com/sqlite/images/logo.png'
    url_5 = 'https://www.tutorialspoint.com/sqlite/images/sqlite-mini-logo.jpg'

    link_1 = ('NULL', url_1, 'u.png', 1, 'READY')
    link_2 = ('NULL', url_2, 'minion', 2, 'READY')
    link_3 = ('NULL', url_3, 'logo', 1, 'READY')
    link_4 = ('NULL', url_4, 'sqliteLogo', 4, 'READY')
    link_5 = ('NULL', url_5, 'sqlite.jpg', 4, 'READY')
    links = (link_1, link_2, link_3, link_4, link_5)

    request_1 = ('NULL', '127.0.0.1/distro/ubuntustudio-18.04.iso', 1)
    request_2 = ('NULL', '127.0.0.1/distro/xubuntu-18.04-desktop-amd64.iso', 0)
    request_3 = ('NULL', '127.0.0.1/distro/ubuntu-18.04-live-server-amd64.iso', 4)
    request_4 = ('NULL', '127.0.0.1/distro/ubuntu-18.04-desktop-amd64.iso', 4)
    request_5 = ('NULL', '127.0.0.1/distro/slax-English-US-7.0.8-x86_64.iso', 2)

    requests = (request_1, request_2, request_3, request_4, request_5)

    connection = sqlite3.connect(DATABASE_FULL_ADDRESS)
    connection.execute("PRAGMA foreign_keys = 1")
    connection.commit()
    cursor = connection.cursor()

    for user in users:
        query = 'INSERT INTO users ' +\
                '(ID, username, password, telegram_ID, schedule_start_time, schedule_stop_time, queue_activation) ' +\
                'VALUES (NULL, "{username}", "{password}", "{tel_id}", "{start}", "{stop}", "{activation}");'\
                .format(username=user[1], password=user[2], tel_id=user[3],
                        start=user[4], stop=user[5], activation=user[6])
        try:
            cursor.execute(query)
        except sqlite3.Error as error:
            debug_msg(error)

    for link in links:
        query = 'INSERT INTO links (id, url, name, owner, status) ' \
                'VALUES (NULL, "{url}", "{name}", "{owner}", "{status}");'\
                .format(url=link[1], name=link[2], owner=link[3], status=link[4])
        try:
            cursor.execute(query)
        except sqlite3.Error as error:
            debug_msg(error)

    for request in requests:
        query = 'INSERT INTO telegram_requests (request_id, request_content, owner) ' \
                'VALUES (NULL, "{request_content}", "{owner}");'\
                .format(request_content=request[1], owner=request[2])
        try:
            cursor.execute(query)
        except sqlite3.Error as error:
            debug_msg(error)

    connection.commit()
    connection.close()


if __name__ == '__main__':
    main()
