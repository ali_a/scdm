#!/usr/bin/env python3

"""
actions needed to be taken in order to add a new user to DB.
"""

from Business_debugger import debug_msg
from Business_auth import Authenticate
from Database_insert_user_info import InsertUserToDatabase


class NewUser:

    @staticmethod
    def add_new_user(username, password):
        is_user_in_database = Authenticate.is_user_in_db(username)
        if is_user_in_database is True:
            debug_msg('New user: user already exists.')
            return False
        else:
            InsertUserToDatabase.insert_user(username, password)
            debug_msg('New user: user added to DB.')
            return True


if __name__ == '__main__':
    NewUser().add_new_user('ali2', '1234')
