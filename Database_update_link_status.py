#!/usr/bin/env python3

"""

"""

from Database_do_update_query import DoUpdateQuery


def set_status_to_done(url_id):
    DoUpdateQuery('UPDATE links SET status = "DONE" WHERE id = "{id}"'.format(id=url_id))


if __name__ == '__main__':
    set_status_to_done(1)
