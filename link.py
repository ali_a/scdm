class Link:
    def __init__(self, link_info):
        self.gid = link_info[0]
        self.url = link_info[1]
        self.owner_ID = link_info[2]

    def get_gid(self):
        return self.gid

    def get_url(self):
        return self.url

    def get_owner_id(self):
        return self.owner_ID
