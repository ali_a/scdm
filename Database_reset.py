#!/usr/bin/env python3

"""
delete the database, and again creates it.
"""

from Database_create import CreateDatabase
from config import DATABASE_FULL_ADDRESS
from Business_debugger import debug_msg
import os

if __name__ == '__main__':
    os.remove(DATABASE_FULL_ADDRESS)
    debug_msg('database removed!')
    CreateDatabase()
