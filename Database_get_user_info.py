#!/usr/bin/env python3

"""
take a username as input and tries to fetch that user's info out of database.
"""

from Database_do_select_query import DoSelectQuery
from Business_debugger import debug_msg

query = 'SELECT * FROM users;'


def get_user_info(user_id):
    try:
        select_result = DoSelectQuery(query).fetch_all()
    except ValueError as error:
        debug_msg(error)
        return -1
    for user in select_result:
        if user[0] == user_id:
            return user


def get_all_users_info():
    try:
        return DoSelectQuery(query).fetch_all()
    except ValueError as error:
        debug_msg(error)
        return -1


if __name__ == '__main__':
    print(get_user_info(1))
