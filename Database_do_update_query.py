#!/usr/bin/env python3

"""

"""

import sqlite3
from Business_debugger import debug_msg
from config import DATABASE_FULL_ADDRESS


# TODO: merge 'Database_do_select_query.py', 'Database_do_insert_query.py' and 'Database_do_update_query.py' together.
class DoUpdateQuery:
    def __init__(self, query):
        if query[0:6].upper() != 'UPDATE':
            raise ValueError('this class only accepts UPDATE queries. request rejected!')

        try:
            self.connection = sqlite3.connect(DATABASE_FULL_ADDRESS)
        except sqlite3.Error as error:
            debug_msg('failed to establish a connection!')
            debug_msg(error)

        try:
            self.cursor = self.connection.cursor()
        except sqlite3.Error as error:
            debug_msg('failed to make the cursor!')
            debug_msg(error)

        try:
            self.cursor.execute(query)
        except sqlite3.Error as error:
            debug_msg('failed to execute the query!')
            debug_msg(error)

        try:
            self.connection.commit()
        except sqlite3.Error as error:
            debug_msg('failed to fetch info!')
            debug_msg(error)

        finally:
            self.cursor.close()
            self.connection.close()


if __name__ == '__main__':
    DoUpdateQuery("UPDATE links SET status = 'READY2' WHERE id = 5;")
