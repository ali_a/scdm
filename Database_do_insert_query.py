#!/usr/bin/env python3

"""
DoInsertQuery's methods:
    0. __init__ : gets a query string, checks if the string starts with 'INSERT' (raise an error if not) and runs the
            query.
"""

import sqlite3
from Business_debugger import debug_msg
from config import DATABASE_FULL_ADDRESS


# TODO: terrible error handling. clean the mess later.
class DoInsertQuery:
    def __init__(self, query):

        if query[0:6].upper() != 'INSERT':
            raise ValueError('this class only accepts INSERT queries. request rejected!')

        try:
            self.connection = sqlite3.connect(DATABASE_FULL_ADDRESS)
        except sqlite3.Error as error:
            debug_msg('failed to establish a connection!')
            debug_msg(error)

        try:
            self.cursor = self.connection.cursor()
        except sqlite3.Error as error:
            debug_msg('failed to make the cursor!')
            debug_msg(error)

        try:
            self.cursor.execute(query)
        except sqlite3.Error as error:
            debug_msg('failed to execute the query!')
            debug_msg(error)

        finally:
            self.connection.commit()
            self.cursor.close()
            self.connection.close()

    def fetch_all(self):
        return self.result


if __name__ == '__main__':
    DoInsertQuery('INSERT INTO users VALUES(NULL, "mamad", "0000")')
