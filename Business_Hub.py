#!/usr/bin/env python3

"""
Hub class is the start point of our program. creates login interface and authenticate the user. then give away the
programs's procedure lead to the Business_main and terminates itself.

It contains the following methods:
    0. __init__ :
        - creates the DB if needed
        - creates a login page onterface called login_page
        - connects login_page's button to proper methods
    1. exit_action : terminate the program.
    2. register_action : TODO
    3. login_action : authenticate the user's information and take the proper action.
    4. get_user_info : get username and password from the constructed login_page (inside __init__) and returns a list
            that contain user's info (['username', 'password'])
"""

import sys
from Interface_login import LoginPage
from PyQt5.QtWidgets import QApplication
from Business_debugger import debug_msg
from Database_create import CreateDatabase
from Business_auth import Authenticate
from Business_main import Main
from Business_new_user import NewUser


class Hub:
    def __init__(self):
        CreateDatabase()
        self.login_page = LoginPage()

        self.login_page.exit_button.pressed.connect(self.exit_action)
        self.login_page.register_button.pressed.connect(self.register_action)
        self.login_page.login_button.pressed.connect(self.login_action)

    @staticmethod
    def exit_action():
        debug_msg('exited from login page.')
        sys.exit()

    def register_action(self):
        username, password = self.get_user_info()
        debug_msg('register!')
        register_result = NewUser().add_new_user(username, password)

        if register_result is None:
            debug_msg('ERROR: Hub: register result is null. This should never happen')
        elif register_result is False:
            self.login_page.prompt_user_exists_and_register_rejected()
        elif register_result is True:
            self.login_page.prompt_user_registered()
        else:
            debug_msg('ERROR: Hub: register result fall all the way down here. register_result = {amount}'
                      .format(amount=register_result))

    def login_action(self):
        username, password = self.get_user_info()
        debug_msg('login!')
        auth_result = Authenticate.user(username, password)

        if auth_result is None:
            debug_msg('ERROR: Hub: auth result is null. This should never happen')
        elif auth_result == 0:
            self.login_page.prompt_user_does_not_exists()
        elif auth_result == -1:
            self.login_page.prompt_wrong_password()
        else:
            self.login_page.close()
            # TODO : terminate this object somehow. no need to keep it in call stack anymore.
            Main(auth_result)

    def get_user_info(self):
        return self.login_page.get_username(), self.login_page.get_password()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    Hub = Hub()
    app.exec_()
