#!/usr/bin/env python3

"""
simply put, creates the database and its tables.

CreateDatabase's __init__ method :
    0. __init__ : take the following actions respectively:
        - calls check_file(). returns if the file already exists.
        - calls create_database()
        - creates a connection to DB (and a cursor, of course)
        - calls create_tables():
            + calls create_table_users()
            + calls create_table_links()
        - calls insert_samples():
            + asks whether to inserts samples to DB or not and take the proper action.
        - commits and closes the connection
"""

import sqlite3
import os.path
import Database_samples
from Business_debugger import debug_msg
from config import DATABASE_FULL_ADDRESS


class CreateDatabase:
    def __init__(self):
        self.file_exists = self.check_file()
        if self.file_exists:
            return
        self.create_database()
        self.connection = sqlite3.connect(DATABASE_FULL_ADDRESS)
        self.cursor = self.connection.cursor()
        self.create_tables()
        self.insert_samples()
        self.connection.commit()
        self.connection.close()

    @staticmethod
    def check_file():
        if os.path.isfile(DATABASE_FULL_ADDRESS):
            debug_msg('Database file exists!')
            return True
        else:
            debug_msg('Database file doesnt exists!')
            return False

    @staticmethod
    def create_database():
        open(DATABASE_FULL_ADDRESS, 'w+')
        debug_msg('Database file created.')

    def create_tables(self):
        self.create_table_users()
        self.create_table_links()
        self.create_table_telegram_requests()

    def create_table_users(self):
        self.cursor.execute('CREATE TABLE users(' +
                            'ID INTEGER PRIMARY KEY AUTOINCREMENT, ' +
                            'username VARCHAR(100) NOT NULL, ' +
                            'password VARCHAR(100) NOT NULL, ' +
                            'telegram_ID VARCHAR(10) NULL, ' +
                            'schedule_start_time TIMESTAMP NULL, ' +
                            'schedule_stop_time TIMESTAMP NULL, ' +
                            'queue_activation INTEGER);')

    # TODO: consider adding status field to this table
    def create_table_links(self):
        self.cursor.execute('CREATE TABLE links(' +
                            'id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
                            'url VARCHAR(500) NOT NULL, ' +
                            'name VARCHAR(100) NULL, ' +
                            'owner INTEGER NOT NULL, ' +
                            'status VARCHAR(10) NOT NULL, ' +
                            'FOREIGN KEY(owner) REFERENCES users(ID));')

    def create_table_telegram_requests(self):
        self.cursor.execute('CREATE TABLE telegram_requests(' +
                            'request_id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
                            'request_content VARCHAR(1000) NOT NULL, ' +
                            'owner INTEGER NOT NULL);')

    @staticmethod
    def insert_samples():
        answer = input('Add some samples to DB ? ')
        if answer in ['Y', 'y', 'yes', 'Yes']:
            Database_samples.main()
            debug_msg('samples added to database.')
        else:
            debug_msg('no samples were added to database.')


if __name__ == '__main__':
    CreateDatabase()
